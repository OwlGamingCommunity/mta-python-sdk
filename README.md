# mta-python-sdk
A Python SDK for Multi Theft Auto designed for [OwlGaming Community](https://owlgaming.net).

This SDK is based of the PHP SDK publicly released for Multi Theft Auto found [on their wiki](https://wiki.multitheftauto.com/wiki/PHP_SDK). The functions in this script match that which is found on the wiki.

## Functions
### callFunction
Calls a specific function.

**Example**
```python
mta = MTA()
print(mta.callFunction('myResouce', 'getServerStats'))
```
This will call and function `getServerStats` in `myResource` and print the returned data.

### getInput
This function is for use with web pages that are called by callRemote.

### doReturn
Use this function when you want to return data when a page is called with callRemote.

## Django
Currently this script is for use with Django and imports the Django settings module to gather information about connecting to the game server. You can easily overwrite this under the initial function in the class.

```python
    def __init__(self, username, password):
        self.http_username = username
        self.http_password = password
        self.host = '127.0.0.1'
        self.port = '22005'
        self.resources = []
```
I included an example above where you can initialize the user and password when creating the class. You would then create the object with:
```python
mta = MTA('website', 'hi')
```

You could of course use this with any webframe work you choose or in scripts to run on pretty much any platform that can run Python to do some fun things with it. 
